## Die Wut, das warme, kraftvolle Gefühl, das die Welt verändern kann

<div class="handwritten">
Was hat dies mit dem Irrgefühl zu tun, von dem du redest.
Ich verstehe nicht so richtig deinen Grundgedanken?
</div>

Nein, ich knüpfe wieder bei dem an.
Das, worüber wir noch nicht geredet haben, ist verdrängte Wut.
Wirklicher Zorn entsteht, wenn in der Wirklichkeit etwas wirklich schief oder schlecht ist.
Dann erzeugt der Körper Wut, ein warmes kraftvolles Gefühl, das dazu führt, dass wir auf all die Kraft, die sonst einfach latent da liegt, Zugang bekommen und sie zum Einsatz kommt, wenn wir krank sind.

Es ist ein warmes und kraftvolles Gefühl der Stärke und des Willens, das, was unvollständig ist, zu berichtigen.
Es zielt nicht auf etwas Herabwertendes oder auf etwas Aggressives gegenüber anderen Menschen oder auf einen selber ab, sondern es wendet sich dem zu, was berichtigt werden soll.
Dieses Gefühl sucht bei anderen Unterstützung, gemeinsames Tun, um in der Krise klarzukommen, egal um welche Schwierigkeit es da geht.
Es bringt kein Irrgefühl hervor.

<div class="handwritten"> 
Wie bitte? Ich dachte, Wut und Aggression sind eins und dasselbe.
Dass es entweder mit der Wut anfängt und dann in Aggression übergeht, oder, dass man bei einer aggressiven Meinung anfängt und dann kommt die Wut als Kraft ... dass ist ja anders als du es beschreibst???
</div>

[//]: # (Frage: Wut und Aggression sind nicht dasselbe?)

Ja, das ist durcheinandergebracht worden, so als wäre es dasselbe, und das ist es nicht.
Wenn die Wut aggressiv wird, und wir reagieren, indem wir beurteilen, verurteilen und bestrafen, uns selber oder andere, so fühlt sich das in dem Moment gut an.
Es wird so wie ein Kick, aber danach kommt das Irrgefühl wegen dem, was wir gesagt und getan haben, und dieses kommt von der Aggressivität, die eine konvertierte Angst ist.

Da wir das Angstgefühl, das der Körper erzeugt hat, nicht entladen, so wird das zu einem Irrgefühl, was an unseren Schuldgefühlen und am unseren schlechten Gewissen zieht und zerrt, und wir genieren uns, wir schämen uns vor uns selber oder vor denen, die wir angegriffen haben.
Dies führt zeitweise dazu, dass wir Abstand nehmen und uns isolieren, um diese Gefühle, die hinter diesen Verhaltensweisen sind, nicht spüren zu müssen, und sie zu entladen.
Ja, das ist ganz anders als die übliche Meinung heutzutage.
Die Vertrautheit mit der Wut und die Geborgenheit darin, wirklich wütend zu werden, wenn sich etwas verfängt, und dann dieses Anregende der zusätzlichen Kraft, von der man nicht weiß, woher sie stammt, das ist sehr erfrischend ... Aber wenn man Angst hat, vor dieser Intimität mit sich selber und in sich selber, die da entsteht, so tun wir stattdessen bewerten, und dann wird das zur Aggressivität und stattdessen unangenehm.

Viele Male verdrängen wir, dieses Eingreifen in die Wirklichkeit zu machen, und schauen stattdessen passiv zu, gerade weil es so viel Freude, Wärme und Licht im Körper schafft, von dieser Kapazität Gebrauch zu machen ... und pfui, es ist so verboten ... Dann werden diese Schüchternheit, dieses Schamgefühl, das Geniert-Sein aufgeweckt, was bewirkt, dass wir uns sündenhaft munter fühlen und dies ist oft mit Scham belegt.
Wir bekommen Angst, dass jemand unsere Munterkeit aus der Wut erblickt und diese muss zu jedem Preis abgedeckt oder ausgelöscht werden und anderen unsichtbar sein.

Wenn wir vor diesem Kraftvollen, das die Wut ist, Angst bekommen, so konvertiert sie also stattdessen zu Aggressivität.
Die Angst schlägt zu, so dass wir dieses lebensbejahende Fühlen nicht kriegen, das eine solche Genugtuung ist, sondern stattdessen werden wir enttäuscht, weil wir leer ausgehen.
Und dann stellen wir Bedingungen auf, beurteilen und verurteilen andere und sind Opfer der Unvollständigkeiten im Leben.
Dies ist es, was unser Selbstvertrauen am meisten bricht.

<div class="handwritten"> 
Aber das ist ja tragisch, dass wir einfach den Deckel darauftun auf all diese Kraft und auf all diese Freude, die so von innen aus uns selber kommen, und ich habe niemanden gehört, der dies so beschrieben hat, so dass ich es verstanden habe.
Wenn du so sagst werde ich verwirrt und verzweifelt.
Es fühlt sich wahr an und ist mir dennoch fremd ... was soll ich damit machen?
</div>

[//]: # (Frage: Wie kann ich mit meiner Wut umgehen?)

Fang damit an, dass du in den Gedanken bei der Wut bist und probiere es aus wenn du alleine vor einem Spiegel stehst, wie das ist, wenn du wütend bist und genieße die Kraft, die du von dir ausstrahlen sehen wirst.
Je gewohnter es dir wird, desto lieber wird dir die Energie der Freude, die sich in deinem Körper ausbreitet.
Je bekannter dir dieser neue Zustand wird, umso besser wird dein Selbstgefühl.
Es löscht das aus, was normalerweise im Weg ist.
