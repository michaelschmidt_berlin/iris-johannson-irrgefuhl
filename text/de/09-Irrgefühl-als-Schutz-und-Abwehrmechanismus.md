## Irrgefühl als Schutz- und Abwehrmechanismus

Ein Irrgefühl ist im Grunde ein Schutz- und Abwehrmechanismus, wenn wir in Lebensgefahr sind, und er schafft, dass wir entgegen aller Wahrscheinlichkeit überleben.
Er wird im Kleinhirn im autonomen Nervensystem erzeugt und die Vernunft ist ihm ganz egal.
Er stellt nur sicher, dass wir auf jeden Fall überleben.

Es fällt uns als Menschen schwer, in der wilden Natur zu überleben.
Wir werden sehr unfertig geboren, und brauchen sehr lange bis wir alleine klarkommen.
Erst mit 10 Jahren haben wir so was, wie eine Fähigkeit, die Alarmmeldung unseres gesunden Gewissens zu spüren, wenn wir uns einer Gefahr nähern.

Wir sind herdenbezogen, und daher haben viele Menschen gemeinsam bessere Voraussetzungen.
Deshalb lebte der Mensch in kleinen Gruppen mit bis etwa 30 erwachsenen Personen in Dorfgemeinschaft.
Man war mit dem Notwendigen beschäftigt, mit dem, was zum Überleben nötig war, und um das hinzukriegen, musste man in die wilde Natur hinausgehen und sich das holen, was man brauchte.

Wenn dann eine Person in eine Situation kam, wo ihr Leben in Gefahr war, so schaltete sich der Autopilot ein, das autonome Schutz- und Abwehrsystem, das nicht über das Bewusstsein geht, sondern direkt aus dem Kleinhirn kommt.
War es dann ein Raubtier, das anzugreifen drohte, so erstarrte die Person ganz, sie blieb in einem Schrecken, in einem Stillstand stecken und dies löste ein Irrgefühl des Körpers aus.
Dann entstand ein Geruch, oder ein Odeur, was oft zur Folge hatte, dass das Raubtier verwirrt und uninteressiert am Menschen als Beute wurde.
Das Raubtier zog ab und der Mensch überlebte.
Danach, wenn der Mensch in Sicherheit war, kam die Entladung ... Der Mensch brach in Gefühlen zusammen und fror, zitterte, weinte, schrie und jammerte, fauchte und knurrte, lachte und weinte, alles durcheinander, bis das Irrgefühl nachließ.
Es konnte alles auf einmal geschehen oder es konnte zeitweise sein.
Es konnte Verzögerung darin geben oder erst viel später auftreten.
Darin gibt es keine Regelmäßigkeit, sondern es ist bei verschiedenen Menschen verschieden und auch bei demselben Menschen unterschiedlich bei verschiedenen Gelegenheiten.

Nachher kommt eine Menge Erinnerungen vom Geschehenen hervor, was bewirkt, dass der Mensch eine erlebte Erfahrung bekommt, die zum Vertrautheitswissen führt und die im Autopiloten bleibt und bei späteren Gelegenheiten aushilft.

Die zweite Reaktion im Fall von Gefahr ist Hochoperativität.
Das heißt, dass die Situation eine solche ist, dass das Raubtier nicht diesen Geruch spüren kann, der ihn verwirrt.
Und da wird die Person stattdessen einen klaren Durchblick bekommen, bei dem alle Information, die zugänglich ist innerhalb des Menschen, und die Information, die in der Atmosphäre ist, Impulse zu Handlungen geben, die dazu führen, dass die Person überlebt.
Zum Beipspiel einen Baum hochklettern, wenn es um ein Raubtier geht, was nicht klettert, sich hinlegen und tot spielen, wenn es ein Tier ist, das nicht sieht, wenn man sich nicht bewegt, sich mit dem Wind im Gesicht, das Tier vor sich verstecken, so dass das Tier nicht den Menschengeruch spürt usw.
Hinterher kommen schlimme Irrgefühlattacken, die sich entladen, wie ich es beschrieben habe, wenn man zur äußeren Geborgenheit kommt, und dadurch lernt man sich, künftig vor solchen Gefahren zu hüten.

Eine ganze Menge dieser Gefährlichkeiten gibt es innerhalb der Menschen, und wenn wir klein sind, so gibt es gewisse ursprüngliche Ängste, die ein Irrgefühl anlaufen lassen, und dann nennt man es Phobien.

Einige bekannte davon sind Offener-Raum-Angst, Platzangst, Reptilien- und Spinnenangst, u.a.
Wenn diese einmal in uns angelaufen sind, können sie unser Leben sehr begrenzen.
Bei dem Stand der Dinge so ist es heute Kognitive Verhaltensmodifikation, die uns da am hilfreichsten sein kann.
Wir werden sie nicht mehr los, aber sie belästigen uns nicht so sehr, und sie stören uns nicht im alltäglichen Leben.

Dabei wird das Irrgefühl in Form von Schutz und Abwehr deutlich, auch wenn es heutzutage nicht sonderlich aktuell ist, da wir in einer Zivilisation leben.
