## Das Lichte und das Dunkle im Menschen

<div class="handwritten">
Warum kriegen wir so viele Gedankenfehler und züchten dadurch so viel Irrgefühl? Irgendwie scheint dies so schief?
</div>

[//]: # (Frage: Warum kriegen wir so viele Gedankenfehler und züchten dadurch so viel unbestimmte. Angst?)

Unwissenheit, meine Liebe.
Es ist kein böser Dämon, der uns Böses will, es ist nur so, dass es kein Fazit gibt und die Zukunft ist unbeschrieben, und deshalb wird etwa die Hälfte falsch, und wir brauchen innehalten und reflektieren und unsere Einsichten und unsere Vertrautheit mit dem Leben vertiefen.
Du weißt, im Primären gibt es die helle Seite, wo wir neugierig und kreativ sind, dort entstehen Möglichkeiten der Veränderung.
Wenn wir aber auf der dunklen Seite landen, dann gibt es nur Ängste, die zum Hemmen und Begrenzen hinsteuern, solches zu verbieten und zu schaffen, dem man selber und andere sich unterordnen sollen ... und es scheint diese Seite zu sein, die die Politik lenkt, und deshalb wird das Gesetzesbuch so absurd dick.
Beachte, dass auch ein Forscher, von dem man als neutral spricht und der nur Fakten vorlegt, einen Kontext hat, innen in sich im Licht zu sein oder im Dunklen, so wie die Theorie eines Philosophen, von dem her gelenkt wird, in welchem Zustand diese Person ist, ob er in lichter Geborgenheit Innen ist oder in dunkler Sorge wegen dem Leben.
Mache dich spielerisch daran, wenn du Leuten zuhörst.

<div class="handwritten">
Ich stecke mich so leicht an Kummer und Sorge anderer an, dass es fast immer bei mir so wird, dass ich Dinge weg haben möchte, allem Elend entkommen und dann nur etwas tun möchte, was mir Freude macht ... aber wenn ich dies tue, bekomme ich ein schlechtes Gewissen und Irrfühlen davon ... und ...???
</div>

Die meisten Forscher sind naturwissenschaftlich geschult und da gibt es fast nichts, was man nicht materiell beweisen kann.
Bis zum Jahr 2012, als die Quantifizierung anerkannt wurde, beschäftigte sich die Naturwissenschaft ausschließlich mit dem Materiellen.
Das Dunkle war nur eine signifikante Abweichung und etwas, von dem man absah.
Deshalb konnte man am Glauben festhalten: Wenn wir bloß alles im Leben beschreiben könnten, dann würden wir das Ideal des persönlichen Glücks erreichen.
Eine Alternative zum Glauben an einem vollendeten Gott.

Ein Forscher ging auf das Dunkle ein und hat aufgezeigt, dass das Materielle nur etwa 5% und das Dunkle, das Nichtmaterielle in Universum 95% ist.
Und er wollte nicht absehen davon und das verursachte ein unbequemes Denken, demgegenüber viele innerhalb der Forscherwelt noch skeptisch sind, von uns gewöhnlichen Menschen ganz abgesehen.
Die Forschung hat als Ziel gehabt, Krankheiten zu bekämpfen und etwa 80% der medizinischen Versorgung kümmert sich um Ängste, die zu Irrgefühl durch Gedanken und Bewertungen geworden sind, und eigentlich nicht durch Beheben der medizinischen Ursachen gelöst werden können, sondern da ist Gesundheitsfürsorge gefragt, Gespräche und das In-der-Nähe-Sein eines mitfühlenden Menschens.

Wenn Leute das im Grunde nicht verstehen, sondern meinen, es geht um die Handhabung von Irrgefühl, nicht darum, es sich auslöschen zu lassen, dann behandelt man Symptome und sieht das als eine Besserung an, und anfangs ist es das ... aber das löst das Problem nicht, schiebt es nur eine Weile auf.
