## Das Irrgefühl ist eine Sprache des Körpers

Irrgefühl ist ein Schutz- und Abwehrmechanismus, da wir auf ungesunde Weise leben.
Wir haben ein Ideal, das der Körper mittels seiner Sprache des Schmerzes versucht uns nahezulegen.
Leider so hören wir sehr schlecht hin und die meisten verstehen die Sprache des Körpers gar nicht, sie sind so weit davon entfernt und da entsteht viel Irrgefühl.
Gerade jetzt wird daran geforscht, wie man das Genom des Alterns manipulieren kann, damit wir ewiges Leben und ewige Gesundheit bekommen sollen.
Ich frage mich, ob das das Irrgefühl im Körper mindern wird?

Der Körper bedient sich aller möglichen Mittel, um uns davon abzuhalten, uns an etwas anzupassen, was von innen her unwohl ist.
Der Körper will, zusammen mit dem Bewusstsein, das Leben mit großem L, und dass das der Sinn ist, das Sein, dieses stille Glück, in der Zufriedenheit zu sein.

Die Geschichte ist schon gewesen und sie ist getan.
Wir können sie zu uns herholen, und von ihr lernen, und unsere Erfahrung an dem wachsen lassen, was nicht so gelungen war.

Die Zukunft ist nur eine Prognose ... das Einzige, was wir von der Zukunft wissen, ist, dass wir nicht wissen, was darin enthalten sein wird, nur dass sie in ständiger Veränderung ist, und nicht durch Ideale, Erwartungen, Forderungen u.a.
zu bestimmen ist.

Das Einzige, dem wird vertrauen können, ist unsere eigene Fähigkeit von innen heraus so zu sein wie wir sind, und dass dies gut genug mit Fehlern, Mängeln, und Verdiensten ist, dass es in dem, was wir schlecht finden etwas Gutes gibt und dass es in dem, was wir gut finden etwas Schlechtes gibt ... so ... es gibt nichts, was ideal, statisch und ewig ist ... wenn dieses nicht tot ist, denn darüber wissen wir ja sowieso nichts, so, darüber können wir unbegrenzte Fantasien haben, damit herumspielen und uns darüber amüsieren oder ganz und gar davon absehen.

Leider sind es viele die diese persönlichen existentiellen Manuskripte haben, die sie ihnen selber verrücken und sie brauchen die äußere Hilfe, von der wir gesprochen haben, und sicherlich gibt es viele weitere Möglichkeiten der Hilfe ...
Alles was notwendig, wirklich notwendig ist, davon wird unser Körper froh, wenn wir uns damit beschäftigen, und da bekommen wir Vertrautheit mit dem Leben selber ... Der Rest ... wenn nichts Notweniges getan werden braucht ... der Rest ist unergründliches Spielen und dieses macht in Beziehung zu Anderen am meisten Spaß.

Wie wir spielen ... hmm ... das liegt an unseren Einfällen, an unserer Persönlichkeit, an der Nahrung für unser Bewusstsein von der Unvollständigkeit her, in Ordnung zu sein, dies nun geborgen und unbewertet.

Eine Frage, die zurzeit in mir lebt ...
Wenn wir ewiges Leben durch die Genmanipulation bekommen, nehmen wir uns dann den Sinn am Leben beim Leben? Wenn dieser wegbleibt, was entsteht an seiner Stelle? ....
Vielleicht Irrgefühl mit großem I? Und dann ... wie lindert man dieses?
Vielleicht dadurch, dass man wieder sterblich wird???

Ich wünsche dir alles Gute beim Auslöschen-Lassen deines eigenen Irrgefühls von innen her, und hole dir gerne Hilfe.
Wir haben es nötig in der Atmosphäre von jemandem zu sein, um die äußere Geborgenheit zu haben, die es möglich macht, dass wir im Entladen in uns selber sein können und das verarbeiten, was hinter und unter dem Irrgefühl ist.

Wenn wir tatsächlich Hilfe bekommen wollen, und wagen uns selber dies zu gönnen, so ist es wie ich schon sagte, möglich, diese Hilfe über den Telefonhörer zu bekommen.
Es gibt diesen Menschen, mit einer einzigartigen Fähigkeit des Zuhörens.
Vielleicht als Ergänzung zu anderem, und manchmal an Stelle der Therapie, wo zwei Stunden so viel kosten, wie die Hilfe über den Hörer monatlich kostet.
Der Vorteil ist, dass du bei dir zu Hause sein kannst, dabei im Bett liegen, im Auto sitzen, im Wald spazieren gehen kannst, und du brauchst keinen anderen Fokus zu haben, als auf das, was in dir selber passiert.

Wenn es nicht möglich sein sollte, damit geborgen zu werden, dass Irrgefühl ungefährlich ist ... lese noch einmal und schau es mit anderen Augen an ... zum Umdenken gibt es viele Möglichkeiten.
Wenn es trotzdem nicht klappt, schreib und erzähle, was in dir passiert, so finden wir es gemeinsam heraus.

Wie ich früher sagte, haben wir Menschen Teile gemeinsam und darin finden wir uns selber wieder, aber wir sind auch einzigartig, besonderes, einzeln, und bei dem gibt es etwas, das nicht vergleichbar ist, und da brauchen wir das Aha-Erlebnis zu finden, das dieses Irrgefühl erlöschen lässt.

Du darfst mir selbstverständlich und gerne schreiben und weiterfragen, wenn du steckenbleibst.
Das ist ein Geschenk, das ich bekomme, das ich mir nie kaufen kann.

Viel Glück
In Freundschaft
iris@irisjohansson.com

<div class="page-break-before">&nbsp;</div>
