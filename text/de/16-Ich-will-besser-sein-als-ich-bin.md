## Ich will besser sein als ich bin

<div class="handwritten">
Kannst du weitere Gedankenfehler aufzählen und etwas darüber erzählen, wie es war vor dem Gedankenfehler, wie Gedankenfehler entstehen, denn ich verstehe nicht wirklich, was sie sind.
Es ist, wie wenn mein Körper sich wehrt, diese Unvollständigkeiten zu begreifen, sie sind irgendwie unlogisch.
</div>

Ja, ich werde einige nennen, die üblich sind und die die meisten bedrücken.

[//]: # (Gedankenfehler: Ich will besser sein als ich bin => Ich bin gut genug wie ich bin und ich übe, was ich nicht kann)

- Dass wir besser sein sollen als wir sind.

Hinter diesem Gedankenfehler ist der Gedanke, der Mensch kann perfekt sein, wenn er bloß vom Anfang an das Richtige tut, und dann würde es keine Unvollständigkeit geben ... der Gedankenfehler ist, dass ohne Unvollständigkeit keine Bewegung und ohne Bewegung kein Leben.
Dann wären wir wie alles im Weltall ... leblos.
Vor seinem Entstehen und der Annahme dieses Gedankenfehlers, waren wir wie Kinder, neugierig auf das, was uns in den Weg kam und wir übten das, woran wir Interesse hatten, bis wir hinreichend gut waren.

Verstehst du den Gedankenfehler? Verstehst du die Unmöglichkeit darin, etwas ist perfekt? Wärest du lieber wie Abfall im Weltall, der sich nie abbaut, nur auf ewig dort ohne Leben schwebt?

<div class="handwritten">
Nein, natürlich nicht, aber ich wäre auch nicht an den Gedanken gekommen, es gäbe Leben ohne Unvollständigkeit.
Wenn ich das weiß, kann ich meine Verwicklungen besser vertragen.
</div>

Ja, so ist es.
Damit hat die innere Konfliktlösung es so auf sich.
Zum Vertragen des Unvollständigen zu kommen und manchmal sogar zu schätzen und froh zu sein, dass nichts perfekt ist, sondern dass wir ständig was zum Ringen haben, sei es damit oder deshalb.

Manchmal brauchen wir Hilfe von außen, und manchmal ist es Hilfe von mir und es ist immer willkommen, wenn du mir schreibst, vielleicht rufst du an, aber es ist oft schwierig, mich zu erreichen ... und manchmal empfehle ich einen Menschen, für den ich Mentorin bin und der eine besondere Begabung darin hat, Menschen am Telefon zuzuhören, sodass sie in Kontakt mit ihren Gefühlen kommen und entladen können.
Das kann uns ein Bedürfnis sein, damit wir im chaotischen Vakuum, das da entsteht, es aushalten, und dass dieses Vakuum sich auslöschen darf und zum Leerraum wird, in dem etwas Neues aufkommen kann, ein Umdenken.
Dies geht oft nicht auf geradem Wege, sondern braucht seine Zeit und hält sich bei Verwicklungen und allen erdenklichen Möglichkeiten des Entkommens auf.
Wir glauben oft, dass es gefährlich ist, hell und zufrieden zu werden, und da sei es uns nicht gegönnt ... Obwohl es das ist, was wir am aller meisten wollen.

Frage einfach bei mir noch nach, so kann ich dich zu ihm weiterleiten, solltet du oder jemand anders das brauchen und wagen auf diese Möglichkeit ohne Risiken einzugehen.

