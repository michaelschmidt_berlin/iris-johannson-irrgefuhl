<div class="impressum">
    <strong>Bitbucket</strong>:
    <!-- if booktype is epub,mobi --><a href="${BOOK_REPOSITORY}">iris-johannson-irrgefuhl</a><br /><!-- endif booktype -->
    <!-- if booktype is pdf --><span>${BOOK_REPOSITORY}</span><!-- endif booktype -->
    (mach mit ;-)<br />
    <!-- if booktype is epub,mobi --><strong>ASIN</strong>: ${BOOK_ASIN}<br /><!-- endif booktype -->
    <!-- if booktype is pdf --><strong>ISBN</strong>: ${BOOK_ISBN}<br /><!-- endif booktype -->
    <strong>Version</strong>: ${BOOK_VERSION}.${BITBUCKET_BUILD_NUMBER}.${BITBUCKET_COMMIT}<br />
    <strong>Lizenz</strong>: ${BOOK_LICENSE}<br />
    <strong>Mitwirkende</strong>:<br /> 
    Filip Wall (Übersetzung)<br />
    Michael Schmidt (Lektorat)<br />
</div>

<div class="title-page">Miteinander Sprechen ... über Irrgefühl.</div>
<div>&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

## Vorwort

Das Heft, das du jetzt vor dir hast, ist entstanden aus der Idee, Gesprächsgruppen zu bilden, die ein Heft lesen und dann darüber sprechen, die Gedanken bewegen, eigene Gedanken hinzufügen, anderen lauschen, sich selber lauschen, sich selber bewegen, sich mit anderen bewegen, die Phänomäne bewegen und bewegen lassen.

Spielen gehört zu dem gemeinsamen Miteinanderbewegen. Auf schwedisch gibt es für das Spielen zwei Wörter. Einmal das Wort „Spela“ (Spielen), das, was man auf einem Musikinstrument tut, oder wenn man gesetzmäßig spielt bei z.B. einem Brettspiel. Dann gibt es das wort „Lek“. Man spricht das Wort aus, um auf das Spielen der Kinder hinzudeuten. Das Kind kommt aus einer Unvoreingenommenheit, es hat keine Bewertungen, hat sein Leben nicht Werten untergeordnet. Es lebt, erlebt seine Umgebung und es tut, erforscht, ahmt nach wie im Spiel, wie wenn es nicht „sein muss“, wie wenn es nichts werden muss. So ist das Wort „Lek“ unbedarft, unvoreingenommen im Leben sein. Das Kind lebt das nicht als Ideal, es lebt sein Leben und Kind und Leben sind am Spielen miteinander.

Michael Schmidt, der die Herausgabe dieses Heftchen mit vielem Drum und Dran besorgt hat, hat den Text in Abschnitte geteilt und jedem Abschnitt mit einer Überschrift versehen. Nachdem ich mit den Wörtern gespielt habe, hat er mit den Überschriften und der grafischen Form gespielt. Der Umschlag war unser gemeinsames Spielen. Wenn ihr gerne den Text in seiner Grundfassung haben wollt, könnt ihr ihn hier herunterladen: ${RAW_BOOK_URL}

Ich habe den Eindruck, dass ihr, die jünger seid, dem Spielen näher seid. Ich merke jedenfalls, dass ich noch viel mehr beim Übersetzen spielen könnte. Spielen kann einen Ernst haben. Viktor Byström und Dieter Schwartz, die sich in das Spielen im Leben vertieft haben, bitten ihre Kursteilnehmer unter anderem zu untersuchen, welche ihre Spiele als sie Kinder waren, waren.
Selber spielte ich viel mit Lego. Mir ging es fast ausschließlich um Raumschiffe, für die die Schwerkraft aufgehoben war. Mir scheint es so, als wäre das Übersetzen — vor allem wenn es vor großen Herausforderungen steht — wie ein Raumschiff, das einen Sprung zu einem anderen Planeten in einen anderen galaktischen Raum macht. Ich liebe diese Sprünge, denn ich komme irgendwie ein wenig anders an, als es war als ich den alten Raum verlassen habe. Aber es ist eine Herausforderung mit den Wörtern von dort, wo man herkommt, aus diesem Sonnensystem mit ihren Planeten und ihrem Leben, die Dinge im neuen Sonnensystem zu beschreiben. Das Leben dort hat seine Stärken und Schwächen und andere sind die Stärken und Schwächen im Ursprung. 

Für mich war es dabei wichtig, Iris Johansson so weit möglich ihre Sprache auch im Deutschen zu lassen. Ich finde ihren Planeten spannend. Ich möchte zunächst nur beschreiben, ihre Eigenartigkeit durchscheinen lassen.
Manchmal habe ich mich und andere zurückgehalten, den Text zu „reinigen“ und ihn allgemeiner, schriftsprachlicher zu machen. Es erschien mir so, dass wenn Iris immer wieder darauf zurückkommt wie unwohl es dem Menschen dabei ist, wenn er danach strebt, besser zu sein als er ist, warum dann versuchen den Text besser zu machen als er ist. Er ist schon gut genug. Anders ist es, wenn man die Form liebt und sie kultivieren möchte. Dabei entfernt man sich aber vom Natürlichen.

Darin besteht wohl das Drama, das die Erde und die Menschheit gemeinsam ausmachen. Dieses Miteinander der menschlichen Kultur und des Natürlichen. Und dann alle Menschen mit ihrer eigenen Kultur und mit ihrem eigenen Natürlichen. 
Das ist wohl das Universum der Gespräche: Ein ganzer Weltraum mit seinen Planeten, um uns. Seid herzlich, kindlich eingeladen durch den Weltraum mit allen anderen Menschen unterwegs zu sein.
Aber nun zu dem Heft und der Frage, was Iris auf ihrem Planeten bewegt.
Und was werdet ihr wohl auf den eurigen bewegen?

Filip Wall, im August 2020
